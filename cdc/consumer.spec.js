const assert = require('assert')
const { Pact } = require('@pact-foundation/pact')
const { fetchTodolist, postTodo, deleteTodo } = require('../src/api')

describe('Pact with todo API', () => {
  const provider = new Pact({
    port: 3001,
    consumer: 'todo-list-front',
    provider: 'todo-list-backend',
    pactfileWriteMode: 'overwrite'
  })

  before(() => provider.setup())

  after(() => provider.finalize())

  describe('When a get request is sent to the Todo API', () => {
    before(async () => {
      return provider.addInteraction({
        state: 'there are todo list',
        uponReceiving: 'a get request for todo list',
        withRequest: {
          path: '/todo',
          method: 'GET',
        },
        willRespondWith: {
          body:
          {
            "todolist": [
              {
                "id": "1",
                "todo": "test 1"
              },
              {
                "id": "2",
                "todo": "test 2"
              },
              {
                "id": "3",
                "todo": "test 3"
              }
            ]
          },
          status: 200,
        },
      })
    })

    it('will receive the list of current todo', async () => {
      fetchTodolist("http://localhost:3001").then((response) => {
        assert.ok(response.status)
      }).catch((error) => {
        console.log(error.message);
      });
    })
  })

  describe('When a post request is sent to the Todo API', () => {
    before(async () => {
      return provider.addInteraction({
        state: 'there are todo',
        uponReceiving: 'a post request for todo',
        withRequest: {
          path: '/todo',
          method: 'POST',
          body: {
            todo: 'Buy some milk'
          }
        },
        willRespondWith: {
          status: 201,
        },
      })
    })

    it("'Buy the milk' to be added to to-do list", async () => {
      postTodo('Buy some milk', "http://localhost:3001").then((response) => {
        assert.ok(response.status)
      }).catch((error) => {
        console.log(error.message);
      });
    })
  })
})


