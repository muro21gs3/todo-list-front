# Todo List Acceptance Tests

## Scenario I want to see the "buy some milk" in list when I input "buy some milk" into the input and press the add button.

* Given Empty ToDo list
* When I write "buy some milk" to "input-todo" and click to "btn-add"
* Then I should see "buy some milk" item in ToDo list