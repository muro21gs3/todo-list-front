import React from "react";
import TodoAdd from "./components/TodoAdd";
import TodoList from "./components/TodoList";
import { connect } from "react-redux";
import { fetchTodolist } from "./api";
import { fetchTodolistSuccess, fetchTodolistFail, fetchTodolistRequest } from "./redux/actions";


class App extends React.Component {
  componentDidMount() {
    var apiBaseUrl = window.API_BASE_URL;
    this.props.fetchTodolistRequest();
    fetchTodolist(apiBaseUrl).then((response) => {
      this.props.fetchTodolistSuccess(response.data);
    })
      .catch((error) => {
        this.props.fetchTodolistFail(error.message);
      });
  }


  render() {
    return (
      <div className="todo-list-add">
        <h2 id="todo-title">Todo List</h2>
        <TodoAdd />
        {this.props.loading ?
          <h3 id="loading">Loading</h3> : this.props.error ?
            <h3 id="errorInfo">{this.props.error}</h3> : <TodoList />}

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.loading,
    error: state.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchTodolistSuccess: (todolist) => dispatch(fetchTodolistSuccess(todolist)),
    fetchTodolistRequest: () => dispatch(fetchTodolistRequest()),
    fetchTodolistFail: (error) => dispatch(fetchTodolistFail(error))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
