import React from "react";
import { mount } from "enzyme";
import App from "./App";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import Reducer from './redux/reducer';

var INITIAL_STATE = {
  loading: false,
  todolist: [],
  error: "",
};

var INITIAL_STATE_LOADING = {
  loading: true,
  todolist: [],
  error: "",
};

const store = createStore(Reducer, INITIAL_STATE, composeWithDevTools(applyMiddleware(thunk)));
const storeLoading = createStore(Reducer, INITIAL_STATE_LOADING, composeWithDevTools(applyMiddleware(thunk)));

describe("Todo List Testing", () => {
  var wrapper;
  beforeEach(() => {
    wrapper = mount(<Provider store={store}><App /></Provider>);
  });

  it("render the title of todo list", () => {
    expect(wrapper.find("h2").text()).toContain("Todo List");
  });

  test("render a button with text of `ADD`", () => {
    expect(wrapper.find("#btn-add").text()).toBe("ADD");
  });

  test("render a texbox with placeholder of `to do`", () => {
    expect(wrapper.find("#input-todo").at(0).props().placeholder).toBe("to do");
  });

  test("render a todo list item with text of `buy some milk`", () => {
    expect(wrapper.find("#todoItem-0").exists())
  });

  test("render the loading of todo list", () => {
    wrapper = mount(<Provider store={storeLoading}><App /></Provider>);
    expect(wrapper.find("#loading").text()).toContain("Loading");
  });
});