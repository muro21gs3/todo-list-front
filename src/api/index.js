const axios = require("axios");

const fetchTodolist = (apiBaseUrl) => axios.get(apiBaseUrl + "/todo")

const postTodo = (content, apiBaseUrl) => axios.post(apiBaseUrl + "/todo", { todo: content })

const deleteTodo = (id, apiBaseUrl) => axios.delete(apiBaseUrl + "/todo/" + id)


module.exports = { fetchTodolist, postTodo, deleteTodo };
