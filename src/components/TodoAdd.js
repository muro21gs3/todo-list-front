import React from "react";
import { useState } from "react";
import { connect } from "react-redux";
import { postTodo } from "../api";
import Button from "../halpers/Button";
import Input from "../halpers/Input";
import { postTodoRequest } from "../redux/actions";

function TodoAdd(props) {
  var apiBaseUrl = window.API_BASE_URL;
  const [value, setValue] = useState("");

  return (
    <div className="todoAdd" id="todoAdd">
      <Input
        id="todo"
        placeholder="to do"
        image="https://icon-library.com/images/to-do-icon/to-do-icon-18.jpg"
        onChange={setValue}
      />
      <Button id="add" onClick={() =>
        postTodo(value, apiBaseUrl).then((response) => {
          props.postTodoRequest(value);
        }).catch((error) => {
          alert(error.message);
        })
      }>ADD</Button>
    </div >
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    postTodoRequest: (todo) => dispatch(postTodoRequest(todo)),

  }
}

export default connect(null, mapDispatchToProps)(TodoAdd);
