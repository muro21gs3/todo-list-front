import React from "react";
import { connect } from "react-redux";
import TodoItem from "../halpers/TodoItem";

function TodoList(props) {
    return (
        <ul className="todoList" id="todoList">
            {props.todoList && props.todoList.map((element, index) =>
                <TodoItem todoId={element.id} todo={element.todo} index={index} key={"todoListItem-" + index} />
            )}
        </ul>
    );
}

const mapStateToProps = (state) => {
    return {
        todoList: state.todolist,
    }
}

export default connect(mapStateToProps)(TodoList);
