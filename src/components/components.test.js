import React from "react";
import { mount } from "enzyme";
import TodoAdd from "./TodoAdd";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import Reducer from '../redux/reducer';
import TodoList from "./TodoList";

var INITIAL_STATE = {
    loading: false,
    todolist: [],
    error: "",
};
const store = createStore(Reducer, INITIAL_STATE, composeWithDevTools(applyMiddleware(thunk)));

describe("Components Testing", () => {
    var wrapper = mount(<Provider store={store}><TodoAdd /><TodoList /></Provider>);

    test("render a TodoAdd componenet", () => {
        expect(wrapper.find("img").at(0).props().src).toBe("https://icon-library.com/images/to-do-icon/to-do-icon-18.jpg");
        expect(wrapper.find("input").at(0).props().id).toBe("input-todo");
        expect(wrapper.find("input").at(0).props().placeholder).toBe("to do");
        expect(wrapper.find("button").at(0).props().children).toBe("ADD");
    })
    test("render a TodoList componenet", () => {
        expect(wrapper.find("#todoList").exists())
    })
});