import React from "react";
import "../style/style.css";

function Button(props) {
    return (
        <button
            id={"btn-" + props.id}
            className="button"
            onClick={props.onClick}
        >
            {props.children}
        </button >
    );
}

export default Button;