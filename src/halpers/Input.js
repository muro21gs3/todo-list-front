import React from "react";
import "../style/style.css";

function Input(props) {
    return (
        <div className="input-wrap">
            <div className="input-border">
                {props.image ? (
                    <img
                        src={props.image}
                        height="20px"
                        className="input-image"
                        alt={props.image + "-input"}
                    />
                ) : (
                    <></>
                )}
                <input
                    id={"input-" + props.id}
                    className="input"
                    placeholder={props.placeholder}
                    type={props.type}
                    onChange={(e) => props.onChange(e.target.value)}
                />
            </div>
        </div>
    );
}

export default Input;