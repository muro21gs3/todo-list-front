import React from "react";

function TodoItem(props) {
    return (
        <li className="todoItem" id={"todoItem-" + props.index}>
            {props.todo}
        </li>
    );
}

export default TodoItem;
