import React, { useState } from "react";
import { mount } from "enzyme";
import Input from "./Input";
import Button from "./Button";
import TodoItem from "./TodoItem";


describe("Halpers Testing", () => {
    test("render a input componenet", () => {
        var onChanged = ""
        function setChange(param) {
            onChanged = param
        }

        var wrapper = mount(
            <Input
                image="https://icon-library.com/images/to-do-icon/to-do-icon-18.jpg"
                id="todo"
                placeholder="to do"
                type="text"
                onChange={setChange}
            />);
        expect(wrapper.find("img").at(0).props().src).toBe("https://icon-library.com/images/to-do-icon/to-do-icon-18.jpg");
        expect(wrapper.find("input").at(0).props().id).toBe("input-todo");
        expect(wrapper.find("input").at(0).props().placeholder).toBe("to do");
        expect(wrapper.find("input").at(0).props().type).toBe("text");
        wrapper.find("input").first().simulate('change', { target: { value: 'onChanged' } });
        expect(onChanged).toBe("onChanged");
    })

    test("render a button componenet", () => {
        var clicked = false
        function click() {
            clicked = true
        }

        var wrapper = mount(<Button id="add" onClick={click}>ADD</Button>);
        expect(wrapper.find("button").at(0).props().id).toBe("btn-add");
        expect(wrapper.find("button").at(0).props().children).toBe("ADD");
        wrapper.find("button").first().simulate("click");
        expect(clicked).toBe(true);
    })

    test("render a todo item componenet", () => {
        var wrapper = mount(
            <TodoItem index="0" todo="buy some milk"> ADD</TodoItem >);
        expect(wrapper.find("li").at(0).props().id).toBe("todoItem-0");
        expect(wrapper.find("li").at(0).props().children).toBe("buy some milk");
    })
});