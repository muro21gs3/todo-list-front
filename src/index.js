import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import Reducer from './redux/reducer';

const store = createStore(Reducer, composeWithDevTools(applyMiddleware(thunk)));

if (window.location.href === "http://34.145.205.40/" || window.location.href === "http://localhost:3000/") {
  window.API_BASE_URL = 'http://34.85.160.252';

} else if (window.location.href === "http://34.152.20.100/") {
  window.API_BASE_URL = 'http://35.203.73.201';
}

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);