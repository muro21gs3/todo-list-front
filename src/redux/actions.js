import {
    POST_TODO_REQUEST,
    FETCH_TODO_LIST_REQUEST,
    FETCH_TODO_LIST_SUCCESS,
    FETCH_TODO_LIST_FAIL,
} from "./types";


const postTodoRequest = (todo) => {
    return {
        type: POST_TODO_REQUEST,
        payload: todo,

    };
};

const fetchTodolistRequest = () => {
    return {
        type: FETCH_TODO_LIST_REQUEST,
    };
};

const fetchTodolistSuccess = (todolist) => {
    return {
        type: FETCH_TODO_LIST_SUCCESS,
        payload: todolist,
    };
};

const fetchTodolistFail = (error) => {
    return {
        type: FETCH_TODO_LIST_FAIL,
        payload: error,
    };
};

export {
    postTodoRequest,
    fetchTodolistRequest,
    fetchTodolistSuccess,
    fetchTodolistFail,
};