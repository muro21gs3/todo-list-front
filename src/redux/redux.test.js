import Reducer from './reducer';
import { postTodoRequest, fetchTodolistRequest, fetchTodolistSuccess, fetchTodolistFail } from "./actions";
import { POST_TODO_REQUEST, FETCH_TODO_LIST_REQUEST, FETCH_TODO_LIST_SUCCESS, FETCH_TODO_LIST_FAIL } from "./types";

var INITIAL_STATE = {
  loading: false,
  todolist: [],
  error: "",
};

describe("Redux Testing", () => {
  test("render a types", () => {
    expect(POST_TODO_REQUEST).toEqual(
      "POST_TODO_REQUEST"
    )
    expect(FETCH_TODO_LIST_REQUEST).toEqual(
      "FETCH_TODO_LIST_REQUEST"
    )
    expect(FETCH_TODO_LIST_SUCCESS).toEqual(
      "FETCH_TODO_LIST_SUCCESS"
    )
    expect(FETCH_TODO_LIST_FAIL).toEqual(
      "FETCH_TODO_LIST_FAIL"
    )
  })
  test("render a actions", () => {
    expect(postTodoRequest("buy some milk")).toEqual(
      { type: POST_TODO_REQUEST, payload: "buy some milk" }
    )
    expect(fetchTodolistRequest()).toEqual(
      { type: FETCH_TODO_LIST_REQUEST }
    )
    expect(fetchTodolistSuccess({ todolist: [{ todo: "buy some milk" }] })).toEqual(
      { type: FETCH_TODO_LIST_SUCCESS, payload: { todolist: [{ todo: "buy some milk" }] } }
    )
    expect(fetchTodolistFail("error")).toEqual(
      { type: FETCH_TODO_LIST_FAIL, payload: "error" }
    )
  })
  test("render a reducer", () => {
    expect(Reducer(INITIAL_STATE, postTodoRequest("buy some milk"))).toEqual(
      {
        loading: false,
        todolist: [{ todo: "buy some milk" }],
        error: ""
      }
    )
    expect(Reducer(INITIAL_STATE, fetchTodolistRequest())).toEqual(
      {
        loading: true,
        todolist: [],
        error: ""
      }
    )
    expect(Reducer(INITIAL_STATE, fetchTodolistSuccess({ todolist: [{ todo: "buy some milk" }] }))).toEqual(
      {
        loading: false,
        todolist: [{ todo: "buy some milk" }],
        error: ""
      }
    )
    expect(Reducer(INITIAL_STATE, fetchTodolistFail("error"))).toEqual(
      {
        loading: false,
        todolist: [],
        error: "error"
      }
    )
  });
});