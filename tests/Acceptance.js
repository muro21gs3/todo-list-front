'use strict';
const {
    goto,
    listItem,
    $,
    click,
    text,
    write,
    waitFor,
} = require('taiko');

const { ENV } = process.env;
const environment = ENV === "local" ? "http://localhost:3000/" : "http://34.145.205.40/";

var lastItemIndex = 0;


step('Given Empty ToDo list', async () => {
    //await openBrowser({ headless: false, args: ["--disable-web-security"] })
    //await openBrowser()
    waitFor(20000)
    await goto(environment)
    waitFor(10000)
    var list = await listItem({ class: "todoItem" }).elements();
    waitFor(10000)
    lastItemIndex = list.length - 1;
    var newItem = `#todoItem-${lastItemIndex + 1}`;
    let newItemExists = await $(newItem).exists();
    waitFor(10000)
    if (newItemExists) {
        throw `Todo list must be empty.`;
    }
});

step('When I write <todo> to <textBox> and click to <addButton>', async (todo, textBox, addButton) => {
    waitFor(20000)
    await write(todo, $(`#${textBox}`))
    waitFor(20000)
    await click($(`#${addButton}`))
});

step('Then I should see <todo> item in ToDo list', async (todo) => {
    waitFor(10000)
    let todoExists = await $(`#todoItem-${lastItemIndex + 1}`).exists();
    if (!todoExists) {
        throw todo + ` should be on the to-do list.`;
    }
});