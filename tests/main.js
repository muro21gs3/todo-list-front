'use strict';
const {
    openBrowser,
    closeBrowser,
} = require('taiko');

beforeScenario(async () => {
    await openBrowser({ args: ['--no-sandbox', '--disable-setuid-sandbox'] });
});

afterScenario(async () => {
    await closeBrowser();
});
